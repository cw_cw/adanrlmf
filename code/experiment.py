# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import pandas as pd
import copy
import csv
import argparse


from nrlmf import NRLMF
from adanrlmf import AdaNRLMF
from adamf import AdaMF

from sklearn.metrics import average_precision_score, precision_recall_curve
from scipy import stats


def load_yamanishi(datasetName, folder):
  dataset = {}
  dataset['name'] = datasetName

  with open(os.path.join(folder, datasetName+"_admat_dgc.txt"), "r") as inf:
    #inf.next()
    inf.readline() #delete column names
    int_array = [line.strip("\n").split()[1:] for line in inf] #[1:] to delete row names

  datasetName = datasetName.replace("_ext", "")
  with open(os.path.join(folder, datasetName+"_simmat_dc.txt"), "r") as inf:  # the drug similarity file
    #inf.next()
    inf.readline()
    drug_sim = [line.strip("\n").split()[1:] for line in inf]

  with open(os.path.join(folder, datasetName+"_simmat_dg.txt"), "r") as inf:  # the target similarity file
    #inf.next()
    inf.readline()
    target_sim = [line.strip("\n").split()[1:] for line in inf]

  drugDrug = np.array(drug_sim, dtype=np.float64)      # drug similarity matrix
  targetTarget = np.array(target_sim, dtype=np.float64)  # target similarity matrix
  drugTarget = np.array(int_array, dtype=np.float64).T    # drug-target label matrix (drugs as rows and proteins as columns)

  dataset['drugDrug'] = drugDrug
  dataset['drugIdToRowIdx'] = np.arange(drugDrug.shape[0])
  dataset['rowIdxToDrugId'] = np.arange(drugDrug.shape[0])
  dataset['targetTarget'] = targetTarget
  dataset['targetIdToColIdx'] = np.arange(targetTarget.shape[0])
  dataset['colIdxToTargetId'] = np.arange(targetTarget.shape[0])
  dataset['drugTarget'] = drugTarget

  return dataset


#first element in the list is the fold for training-test split. The remaining
#folds are for K-fold cross-validation
def load_trials(trial_folder, num_trials):
 
  trial_list = []
  for trial_idx in range(num_trials):
    file_name = "trial_" + str(trial_idx)

    trial = []
    with open(os.path.join(trial_folder, file_name), "r") as reading_file:
      for line in reading_file:
        two_lists = line.strip("\n").split(";")

        fold = {}
        drug_idx = two_lists[0].split(",")
        fold["drug_idx"] = np.array(drug_idx, dtype=int)
        target_idx = two_lists[1].split(",")
        fold["target_idx"] = np.array(target_idx, dtype=int)

        trial.append(fold)

    trial_list.append(trial)

  return trial_list


def create_learner(learner_name, seed):

  if(learner_name == 'nrlmf'):
    learner = NRLMF(seed=seed)
  elif(learner_name == 'adanrlmf'):
    learner = AdaNRLMF(seed=seed)
  elif(learner_name == 'adamf'):
    learner = AdaMF(seed=seed)
  else:
    print("learner %s not found" % (learner_name))
    sys.exit(1)

  return learner


def make_seeds(num_seeds = 10, start=6):

  seeds = np.logspace(start=start, stop=32, num=num_seeds, endpoint=False, base=2.0)

  seeds = [ np.uint32(seed) for seed in seeds ]

  return seeds


#check if a whole row/col have only np.nan values and returns the
#those row/col indexes
def whereRowColNan(matrix):
  noNanRow, noNanCol = np.where(np.isnan(matrix) == False)

  numRows, numCols = matrix.shape

  rowIndeces = range(numRows)
  colIndeces = range(numCols)

  rowToDelete = list(set(rowIndeces) - set(noNanRow))
  colToDelete = list(set(colIndeces) - set(noNanCol))

  return rowToDelete, colToDelete;


#Remove the fold data on matrix by changing their value to np.nan according to the cv_setting_op
def foldDataToNan(matrix, fold, cv_setting_op):

  drug_idx = fold['drug_idx']
  target_idx = fold['target_idx']

  if(cv_setting_op == 1):
    matrix[ drug_idx, target_idx ] = np.nan

  elif(cv_setting_op == 2): #deleting drug rows
    matrix[drug_idx, :] = np.nan

  elif(cv_setting_op == 3): #deleting target cols
    matrix[:, target_idx] = np.nan

  elif(cv_setting_op == 4): #deleting drug-target cols-rows
    matrix[drug_idx, :] = np.nan
    matrix[:, target_idx] = np.nan
  else:
    print("Incorrect cv_setting_op option")

  return matrix


def training_test_pair_list(training_set, out_fold_list , cv_setting_op, keep_dimension=True):

  pair_list = []
  for out_fold in out_fold_list:
    pair_list.append( training_test_pair(training_set, out_fold, cv_setting_op, keep_dimension) )

  return pair_list


#Split a dataset between training (all data less evaluation fold data)
#and test set (whole matrix)
def training_test_pair(trainingSet, out_fold , cv_setting_op, keep_dimension=True): 
  dataRow, dataCol = np.where( np.isnan(trainingSet['drugTarget']) == False )

  newTrainingSet = {}
  testSet = {}

  num_drugs = len(trainingSet['rowIdxToDrugId'])
  num_targets = len(trainingSet['colIdxToTargetId'])

  trainingDD = trainingSet['drugDrug'].copy()
  trainingDD_ids = trainingSet['rowIdxToDrugId'][:]
  trainingTT = trainingSet['targetTarget'].copy()
  trainingTT_ids = trainingSet['colIdxToTargetId'][:]
  trainingDT = trainingSet['drugTarget'].copy()

  testDD = trainingSet['drugDrug'].copy()
  testDD_ids = trainingSet['rowIdxToDrugId'][:]
  testTT = trainingSet['targetTarget'].copy()
  testTT_ids = trainingSet['colIdxToTargetId'][:]
  testDT = trainingSet['drugTarget']

  trainingDT = foldDataToNan(trainingDT, out_fold, cv_setting_op)
  drugToDelete, targetToDelete = whereRowColNan(trainingDT)

  newTrainingSet['drugDrug'] = trainingDD
  new_drug_ids = list(set(range(num_drugs)) - set(drugToDelete) )
  newTrainingSet['rowIdxToDrugId'] = trainingDD_ids[new_drug_ids]

  newTrainingSet['targetTarget'] = trainingTT
  new_target_ids = list(set(range(num_targets)) - set(targetToDelete) )
  newTrainingSet['colIdxToTargetId'] = trainingTT_ids[new_target_ids]
  newTrainingSet['drugTarget'] = trainingDT

  testSet['drugDrug'] = testDD
  testSet['rowIdxToDrugId'] = testDD_ids
  testSet['targetTarget'] = testTT
  testSet['colIdxToTargetId'] = testTT_ids
  testSet['drugTarget'] = testDT

  return (newTrainingSet, testSet)


def crossvalidation(dataset, trial_list, cv_setting_op, param_range, learner_name, perf_metric, is_ensemble, seed_list):

  result_table = pd.DataFrame()
  ensemble_detail = ""

  if(learner_name == "adanrlmf"):#load base learner optimal parameters for adanrlmf
      
      with open("param_" + dataset["name"] + "_cv"+ str(cv_setting_op)+ ".csv") as csv_file:
        best_param_list = [{key: float(value) for key, value in row.items()} for row in csv.DictReader(csv_file)]
      
      validation_score = 0.42 #some default value 

  for trial_idx in range(len(trial_list)):
    trial = trial_list[trial_idx]

    test_fold = trial[0] #first fold is testing fold
    training_set, test_set = training_test_pair(dataset, test_fold, cv_setting_op)

    learner = create_learner(learner_name, seed_list[trial_idx])

    if learner_name == "adanrlmf":
      best_param = best_param_list[trial_idx]
      best_param["num_models"] = param_range["num_models"]
      
    else:  #get optimal parameters by cross-validation procedure
      val_fold_list = trial[1:]
      training_test_list = training_test_pair_list(training_set, val_fold_list, cv_setting_op)
      best_param, validation_score = learner.param_optimization(training_test_list, param_range, val_fold_list, perf_metric, "bo_mi")

    #train using optimal parameters
    learner.train(training_set, best_param)

    #evaluate prediction performance
    test_drug_idx = test_fold["drug_idx"]
    test_target_idx = test_fold["target_idx"]

    test_mat = test_set['drugTarget']
    true_labels = test_mat[test_drug_idx, test_target_idx]

    prediction = learner.predict(test_set)
    prediction = prediction[test_drug_idx, test_target_idx]

    predict_score = perf_metric(true_labels, prediction)

    row_result_table = best_param
    row_result_table["Validation_score"] = validation_score
    row_result_table["Prediction_score"] = predict_score

    result_table = result_table.append(row_result_table, ignore_index=True)
    
    if is_ensemble: #get performace information for every new base learner added to the ensemble
      train_drug_idx, train_target_idx = np.where(learner.M == True)

      ensemble_train = np.array(learner.progressive_evaluation(training_set["drugTarget"], perf_metric, train_drug_idx, train_target_idx))
      ensemble_test = np.array(learner.progressive_evaluation(test_set["drugTarget"], perf_metric, test_drug_idx, test_target_idx))

      np.set_printoptions(precision=6)
      np.set_printoptions(suppress=True)
      detail_train = "Train scores: \n{}, {}".format(ensemble_train, learner.seed)
      detail_test  = "Test scores: \n{}, {}".format(ensemble_test, learner.seed)

      ensemble_detail += "\n".join([detail_train, detail_test]) + "\n"

  avg_val_score = result_table["Validation_score"].mean()
  std_val_score = result_table["Validation_score"].std()
  avg_pred_score = result_table["Prediction_score"].mean()
  std_pred_score = result_table["Prediction_score"].std()

  result_table.index.name = "Trials"

  return result_table, avg_val_score, std_val_score, avg_pred_score, std_pred_score, ensemble_detail


def save_results(data, args, is_ensemble):

  file_name = "cv{}_{}_{}_trials-{}.txt".format(args['cvSetting'], args['dataSetName'], args['methodName'], args['cvTrials'])

  writting_file = open(file_name, 'w+')

  writting_file.write(data['table'])
  writting_file.write("\n")
  writting_file.write(data['avg'])
  writting_file.write("\n")
  writting_file.write( "Parameter Range" + str(args['paramRange']) )
  if is_ensemble:
    writting_file.write("\n\n")
    writting_file.write(args['ensembleDetail'])

  writting_file.close()


def parse_arguments(argv):

  _MIN_PARAMTERS_ = 3

  parser = argparse.ArgumentParser(prog="experiments.py", usage='python %(prog)s <learner_name> <dataset_name> <cv_setting> [optional arguments]')

  parser.add_argument("learner_name", metavar="learner_name",
    choices=["nrlmf", "adanrlmf", "adamf"],
    type=str,
    help="learner method to predict drug-target intractions. Avaible options are: nrlmf, adamf and adanrlmf")

  parser.add_argument("dataset_name", metavar="dataset_name",
    choices=['nr', 'gpcr', 'ic', 'e','nr_ext', 'gpcr_ext', 'ic_ext', 'e_ext'],
    type=str,
    help="Dataset name to use for experimentation. Dataset names are: nr, gpcr, ic, e, nr_ext, gpcr_ext, ic_ext and e_ext.")

  parser.add_argument("cv_setting", metavar="cv_setting",
    choices=[1,2,3,4],
    type=int,
    help="Setting for cross-validation. Setting 1 randomly hides pair drug-target, setting 2 randomly hides rows (drugs), setting 3 randomly hides cols (targets) and setting 4 randomly hides rows and cols (drugs and targets). If you want to use other setting than 1, make their corresponding folds with 'make_folds.py' program (see options with 'make_folds.py -h').")

  parser.add_argument("--num_trials",
    default=10,
    type=int,
    help="Number of trials for cross-validation. Must be less or equal amount of trial files for a given dataset. If you want to make bigger trial, run 'make_folds.py' with corresponding parameters (see options with 'make_folds.py -h')")

  parser.add_argument("--perf_metric",
    default="aupr",
    choices=['aupr'],
    help="Performance metric used for evaluation and optimization process. Only metric avaible is AUPR")

  parser.add_argument("--dataset_directory",
    default="../dataset/yamanishi/",
    type=str,
    help="Relative or absolute path to dataset directory. Default '../dataset/yamanishi/'")

  parser.add_argument("--save_output",
    action="store_true",
    help="Enable save output to disk (in a safe way). File format is cv-setting_dataset-name_cv-type_num-seed.txt.")

  parser.add_argument("--save_directory",  default=".", type=str,
    help="Relative or absolute path to save results from experiments. Default: current path")

  parser.add_argument("--detail",
    help="Print results for every cross validation fold and their corresponding parameters. Also, the global average and std.",
    action="store_true")

  if len(argv) < _MIN_PARAMTERS_:
    parser.print_help()
    sys.exit

  args = parser.parse_args()

  return args


def main(argv):

  args = parse_arguments(argv)

  if args.perf_metric == 'aupr':
    perf_metric = average_precision_score
  else:
    print("Error: performance metric " + args.perf_metric +"not found.")
    exit(1)

  dataset = load_yamanishi(args.dataset_name, args.dataset_directory)
  
  trial_folder = os.path.join("cv"+str(args.cv_setting), args.dataset_name)
  trial_list = load_trials(trial_folder, args.num_trials)
  
  seed_list = make_seeds(args.num_trials)

  param_range = {}
  
  keepDimension = True
  saving_output = True
  is_ensemble = False
  
  #Learner parameters space 
  if(args.learner_name == 'nrlmf'):

    '''
    #short test parameters
    param_range['c'] = np.array([5, 6, 1])
    param_range['k1'] = np.array([5, 6, 1])
    param_range['k2'] = np.array([5, 6, 1])
    param_range['num_factors'] = np.array([50, 51, 50])
    param_range['lambda'] = np.array([0, 1, 1])
    param_range['lambda_u'] = np.array([0, 1, 1])
    param_range['lambda_v'] = np.array([0, 1, 1])
    param_range['gamma'] = np.array([0, 1, 1])
    ''' 
    #Defaul NRLMF parameter space 
    param_range['c'] = np.array([5, 6, 1])
    param_range['k1'] = np.array([5, 6, 1])
    param_range['k2'] = np.array([5, 6, 1])
    param_range['num_factors'] = np.array([50, 101, 50])
    param_range['lambda'] = np.array([-5, 2, 1])
    param_range['lambda_u'] = np.array([-5, 3, 1])
    param_range['lambda_v'] = np.array([-5, 1, 1])
    param_range['gamma'] = np.array([-3, 1, 1])
    #'''
    
    param_range['max_iter'] = 100
    
    keep_dimension = True

  elif(args.learner_name == 'adanrlmf'):
    
    '''
    #short test parameters
    param_range['c'] = np.array([5, 6, 1])
    param_range['k1'] = np.array([5, 6, 1])
    param_range['k2'] = np.array([5, 6, 1])
    param_range['num_factors'] = np.array([50, 51, 50])
    param_range['lambda'] = np.array([0, 1, 1])
    param_range['lambda_u'] = np.array([0, 1, 1])
    param_range['lambda_v'] = np.array([0, 1, 1])
    param_range['gamma'] = np.array([0, 1, 1])
    ''' 
    #Defaul NRLMF parameter space 
    param_range['c'] = np.array([5, 6, 1])
    param_range['k1'] = np.array([5, 6, 1])
    param_range['k2'] = np.array([5, 6, 1])
    param_range['num_factors'] = np.array([50, 101, 50])
    param_range['lambda'] = np.array([-5, 2, 1])
    param_range['lambda_u'] = np.array([-5, 3, 1])
    param_range['lambda_v'] = np.array([-5, 1, 1])
    param_range['gamma'] = np.array([-3, 1, 1])
    #'''
    
    param_range['num_models'] = 10
    param_range['max_iter'] = 100
    
    keep_dimension = True
    is_ensemble = True    

  elif(args.learner_name == 'adamf'):
     
    #Defaul parameters for AdaMF
    param_range['num_factors'] = np.array([100, 101, 1])
    param_range['lambda_reg'] = np.array([-1, 0, 1])
    param_range['gamma'] = np.array([-3, -2, 1])

    param_range['num_models'] = 10
    param_range['max_iter'] = 100

    keep_dimension = True
    is_ensemble = True

  else:
    print("Parameter range for learner '"+ args.learner_name +"' not found.")
    sys.exit(1)

  ############### run experiments ###################

  result_table, avg_val_score, std_val_score, avg_pred_score, std_pred_score, ensemble_detail = crossvalidation(dataset, trial_list, args.cv_setting, param_range, args.learner_name,  perf_metric, is_ensemble, seed_list)
  
  #format output as pandas table
  table_str = result_table.to_string(float_format=lambda x: '%10.6f' % x)
  avgResults = "training_avg: %.6f (%.6f) \t predict avg: %.6f (%.4f)" % (avg_val_score, std_val_score, avg_pred_score, std_pred_score)
  
  #save optimal parameters of nrlmf for adanrlmf use
  if(args.learner_name == 'nrlmf'):
    result_table.to_csv("param_" + args.dataset_name + "_cv" + str(args.cv_setting) + ".csv", index=False) #save parameters as csv
  
  if(args.detail == True):
    if is_ensemble:
      print(ensemble_detail)

    print("Learner : " + args.learner_name)
    print("Setting : " + str(args.cv_setting))
    print("Dataset : " + args.dataset_name)
    print("\n")

    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
      print(result_table)
    print("\n")
    print('Range Parameters:' + str(param_range))
    print(avgResults)

  if args.save_output == True :
    saveData = {}
    saveArgs = {}
    saveData['table'] = table_str
    saveData['avg'] = avgResults

    saveArgs['cvSetting'] = args.cv_setting
    saveArgs['dataSetName'] = args.dataset_name
    saveArgs['methodName'] = args.learner_name
    saveArgs['cvTrials'] = args.num_trials
    saveArgs['evalMet'] = args.perf_metric
    saveArgs['paramRange'] = param_range

    if is_ensemble:
      saveArgs['ensembleDetail'] = ensemble_detail

    save_results(saveData, saveArgs, is_ensemble)

  ## Print output, it is used from "run_all_experiments.py" to produce summary tables
  print (avg_pred_score, std_pred_score)

if __name__=="__main__":
  main(sys.argv)
