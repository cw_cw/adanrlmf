import numpy as np
# from joblib import Parallel, delayed
from sklearn.metrics import average_precision_score, precision_recall_curve


def aupr(y_true, y_score, sample_weight=None):
  precision, recall, thresholds = precision_recall_curve(
      y_true, y_score, sample_weight=sample_weight)
  # Return the step function integral
  # The following works because the last entry of precision is
  # guaranteed to be 1, as returned by precision_recall_curve
  return -np.sum(np.diff(recall) * np.array(precision)[:-1])


class AdaNRLMF:

  def __init__(self, c= 5, k1 = 5, k2 = 5, num_factors=50, lambda_reg=0.625, lambda_u=0.1, lambda_v = 0.1, gamma=1, max_iter=50, num_models = 2, seed=42):

    #NRLMF paramters
    self.c = int(c)  # importance level for positive observations
    self.k1 = int(k1)
    self.k2 = int(k2)
    self.num_factors = int(num_factors)
    self.lambda_d = 2.**float(lambda_reg)
    self.lambda_t = 2.**float(lambda_reg)
    self.lambda_u = 2.**float(lambda_u)
    self.lambda_v = 2.**float(lambda_v)
    self.gamma = 2.** float(gamma) #learning rate used in AdaGrad optimization
    self.max_iter = int(max_iter)

    #ensemble paramters
    self.num_models = int(num_models)

    #General paramters
    self.prng = np.random.RandomState(seed)
    self.seed = seed #for debugging
    self.epsilon = 1e-7


  def init_sample_weights(self):

    self.sample_weights = self.M.copy()
    self.sample_weights = self.sample_weights / float(np.sum(self.sample_weights))
    
    # self.acum_denominator = self.M.copy()


  def construct_neighborhood(self, drug_sim_mat, target_sim_mat):

    self.drug_sim_zeroDiag = drug_sim_mat - drug_sim_mat*np.eye(self.num_drugs)
    self.target_sim_zeroDiag = target_sim_mat - target_sim_mat*np.eye(self.num_targets)

    #drug/target_k1_NN are 'A' and 'B' respectively in the paper of NRLMF.
    drug_k1_NN = self.get_nearest_neighbors(self.drug_sim_zeroDiag, self.k1)
    self.drug_lapacian = self.laplacian_matrix(drug_k1_NN)

    target_k1_NN = self.get_nearest_neighbors(self.target_sim_zeroDiag, self.k1)
    self.target_laplacian = self.laplacian_matrix(target_k1_NN)


  def laplacian_matrix(self, S):
    colum_sum = np.sum(S, axis=0)
    row_sum = np.sum(S, axis=1)
    L = 0.5*( np.diag(colum_sum + row_sum) - ( S + S.T) ) # neighborhood regularization matrix %simulate laplacian matrix for undirect graphs

    return L


  def get_nearest_neighbors(self, S, nun_neighbors = 5):
    m, n = S.shape
    X = np.zeros((m, n))
    for i in range(m):
      ii = np.argsort(S[i, :])[::-1][:min(nun_neighbors, n)]
      X[i, ii] = S[i, ii]

    return X


  def set_parameters(self, parameters):

    self.c = int(parameters['c'])
    self.k1 = int(parameters['k1'])
    self.k2 = int(parameters['k2'])
    self.num_factors = int(parameters['num_factors'])
    self.lambda_d = 2. ** parameters['lambda']
    self.lambda_t = 2. ** parameters['lambda']
    self.lambda_u = 2. ** parameters['lambda_u']
    self.lambda_v = 2. ** parameters['lambda_v']
    self.gamma = 2. **parameters['gamma']
    self.max_iter = int(parameters['max_iter'])

    self.num_models = int(parameters['num_models'])


  def log_likelihood(self, U, V):

    u_vt = U.dot(V.T)
    first_term = self.c * self.interact_mat * self.masked_weights * u_vt
    loglik = np.sum(first_term)
    
    drug_weights = np.ones(U.shape[0])
    target_weights = np.ones(V.shape[0])
    
    #second equation term: (1 + (c-1) y_{ij}) log( 1 + exp(u_i v_j^t )
    log_1_exp_u_vt = np.log(1 + np.exp(u_vt))
    second_term = (1 + (self.c - 1) * self.interact_mat) * log_1_exp_u_vt * self.masked_weights
    loglik -= np.sum(second_term)

    #LMF regularized term (3th and 4th equation terms)
    loglik -= 0.5 * self.lambda_d * np.sum(np.square(U))
    loglik -= 0.5 * self.lambda_t * np.sum(np.square(V))
    
    #NRLMF drug neighborhood regularized term (5th equation term)
    loglik -= 0.5 * self.lambda_u * np.sum(np.diag(U.T.dot(self.drug_lapacian).dot(U)))
    
    #NRLMF target neighborhood regularized term (6th equation term)
    loglik -= 0.5 * self.lambda_v * np.sum(np.diag(V.T.dot(self.target_laplacian).dot(V)))

    return loglik
    

  def deriv(self, U, V, make_drug_deriv = True):

    uv_t = U.dot(V.T)
    P = self.logistic_function(uv_t)
    c_1_Y_1_P_M_W = ((self.c - 1) * self.interact_mat + 1) * P * self.masked_weights 
    
    drug_weights = np.ones(U.shape[0])
    target_weights = np.ones(V.shape[0])
    
    if make_drug_deriv:
      vec_deriv = self.c * (self.interact_mat * self.masked_weights).dot(V)
      vec_deriv -= c_1_Y_1_P_M_W.dot(V)
      vec_deriv -= self.lambda_d * U #LMF reg. term
      vec_deriv -= self.lambda_u * self.drug_lapacian.dot(U) #NRLMF reg. term

    else:
      vec_deriv = self.c * (self.interact_mat * self.masked_weights).T.dot(U)
      vec_deriv -= c_1_Y_1_P_M_W.T.dot(U)
      vec_deriv -= self.lambda_t * V
      vec_deriv -= self.lambda_v * self.target_laplacian.dot(V)

    return vec_deriv

  def AGD_optimization(self, U, V):
    acum_drug_deriv = np.zeros((self.num_drugs, self.num_factors))
    acum_target_deriv = np.zeros((self.num_targets, self.num_factors))

    last_log = self.log_likelihood(U, V)
    for iter_idx in range(self.max_iter):

      #Calc of learning rate for U using AdaGrad
      drug_derivative = self.deriv(U, V, make_drug_deriv = True)
      acum_drug_deriv += np.square(drug_derivative) 
      vec_step_size = self.gamma / np.sqrt(acum_drug_deriv)

      #Fix V and update U
      U += vec_step_size * drug_derivative 

      #Calc of learning rate for V using AdaGrad
      target_derivative = self.deriv(U, V, make_drug_deriv = False)
      acum_target_deriv += np.square(target_derivative)
      vec_step_size = self.gamma / np.sqrt(acum_target_deriv)

      #Fix U and update V
      V += vec_step_size * target_derivative

      #check for minimal increment stop criteria
      curr_log = self.log_likelihood(U, V)

      delta_log = (curr_log-last_log)/float(abs(last_log))
      if abs(delta_log) < 1e-5:
          break
      last_log = curr_log

    return iter_idx
    

  def repair_latent_vectors(self, U, V):
    drug_idxs = np.array(list(self.train_drugs))
    drugSim_zeroDiag_train = self.drug_sim_zeroDiag[:, drug_idxs]

    target_idxs = np.array(list(self.train_targets))
    targetSim_zeroDiag_train = self.target_sim_zeroDiag[:, target_idxs]

    drugs = range(self.num_drugs)
    targets = range(self.num_targets)

    drugs_to_repair = list(set(drugs) - self.train_drugs )
    targets_to_repair = list(set(targets) - self.train_targets )

    for drug_idx in drugs_to_repair:
      #get the k2 most similar drugs
      drug_k2_NN = np.argsort(drugSim_zeroDiag_train[drug_idx, :])[::-1][:self.k2]

      #get their corresponding similarity
      drug_k2_NN_sim = np.asmatrix(drugSim_zeroDiag_train[drug_idx, drug_k2_NN]) #dimension of 1 x k2

      #get the new latent vector as the weighted average of its k2 most similar
      #neighbors
      U[drug_idx, :] = drug_k2_NN_sim.dot(U[drug_idxs[drug_k2_NN], :])
      U[drug_idx, :] = U[drug_idx, :] / np.sum(drug_k2_NN_sim)

    for target_idx in targets_to_repair:
      target_k2_NN = np.argsort(targetSim_zeroDiag_train[target_idx, :])[::-1][:self.k2]
      target_k2_NN_sim = np.asmatrix(targetSim_zeroDiag_train[target_idx, target_k2_NN])
      V[target_idx, :] = target_k2_NN_sim.dot(V[target_idxs[target_k2_NN], :])
      V[target_idx, :] = V[target_idx, :] / np.sum(target_k2_NN_sim)


  def logistic_function(self, x):
    return 1.0 / (1.0 + np.exp(-x))


  def progressive_evaluation(self, interact_test, perf_metric, drug_test_idxs = None, target_test_idxs = None):
    scores = []
    strong_learner = np.zeros((self.num_drugs, self.num_targets))
    true_labels = interact_test.ravel()
    
    acum_alpha = 0.0
    for base_learner_id in range(self.num_models):
      U = self.U_list[base_learner_id]
      V = self.V_list[base_learner_id]
      weak_prediction = self.logistic_function(U.dot(V.T))
      
      logit_pred = 0.5 * np.log((weak_prediction + self.epsilon)/(1.0 - weak_prediction + self.epsilon))

      alpha = self.base_learner_weights[base_learner_id]
      
      acum_alpha += alpha
      strong_learner += alpha * logit_pred

      strong_pred_1d = strong_learner.ravel() / acum_alpha
      norm_pred = strong_learner / acum_alpha

      if(not (isinstance(drug_test_idxs, list) or isinstance(drug_test_idxs, np.ndarray)) ):
        scores.append(perf_metric(true_labels, strong_pred_1d))
      else:
        scores.append(perf_metric(interact_test[drug_test_idxs, target_test_idxs], norm_pred[drug_test_idxs, target_test_idxs]))

    return  scores


  def train(self, training_set, parameters=None, test_set=None, test_fold=None):
    
    if(isinstance(parameters, dict)):
      self.set_parameters(parameters)
    
    self.M = np.isfinite(training_set['drugTarget'])
    num_train_samples = np.sum(self.M)
    train_idxs = np.where(self.M.ravel() == True)[0]
    
    self.interact_mat = np.nan_to_num(training_set['drugTarget'])
    drug_sim_mat = training_set['drugDrug']
    target_sim_mat = training_set['targetTarget']
    
    scaled_true_label_mat = 2.0 * self.interact_mat - 1.0 #scaled to {-1, 1}
    scaled_true_label_1d = scaled_true_label_mat.ravel()
    scaled_train_label = scaled_true_label_1d[train_idxs]
    
    self.num_drugs, self.num_targets = training_set['drugTarget'].shape

    interact_drugs, interact_targets = np.where(self.interact_mat > 0)
    self.train_drugs = set(interact_drugs.tolist())
    self.train_targets = set(interact_targets.tolist())

    self.construct_neighborhood(drug_sim_mat, target_sim_mat)

    mean = 0.0
    std_d = 1.0 / np.sqrt(self.lambda_d)
    std_t = 1.0 / np.sqrt(self.lambda_t)
    
    self.U_list = [np.sqrt(1/float(self.num_factors)) * self.prng.normal(size=(self.num_drugs, self.num_factors), loc = mean, scale=std_d)  for i in range(self.num_models)]
    self.V_list = [np.sqrt(1/float(self.num_factors)) * self.prng.normal(size=(self.num_targets, self.num_factors), loc = mean, scale=std_t) for i in range(self.num_models)]

    self.base_learner_weights = np.zeros(self.num_models)
    self.strong_learner = np.zeros((self.num_drugs, self.num_targets))
    self.init_sample_weights()
    self.masked_weights = num_train_samples * self.M * self.sample_weights

    for base_learner_id in range(self.num_models):
      U = self.U_list[base_learner_id]
      V = self.V_list[base_learner_id]
      
      self.AGD_optimization(U,V)
      self.repair_latent_vectors(U,V)
      
      prediction = self.logistic_function(U.dot(V.T))
      
      logit_pred = 0.5 * np.log((prediction + self.epsilon)/(1.0 - prediction + self.epsilon))
      train_logit_preds = logit_pred.ravel()[train_idxs]
      
      sample_weights_1d = self.sample_weights.ravel()
      
      self.base_learner_weights[base_learner_id] = 1.0

      self.strong_learner +=  logit_pred
      
      sample_weights_1d[train_idxs] = sample_weights_1d[train_idxs] * np.exp(-scaled_train_label * train_logit_preds)
      sample_weights_1d[train_idxs] = sample_weights_1d[train_idxs] / np.sum(sample_weights_1d[train_idxs])
      
      self.masked_weights = num_train_samples * self.M * self.sample_weights
      

  def make_range(self, parameter_range):
    return np.arange(parameter_range[0], parameter_range[1], parameter_range[2])

  
  #posible optimization: pre-calculate nearest neaighbours and give it as argument
  def eval_model(self, train_test_pair, val_fold, c, k1, k2, num_factors, lambda_reg, lambda_u, lambda_v, gamma, max_iter, num_models, seed, perf_metric):
    
    train_set, test_set = train_test_pair
    test_drug_idx = val_fold["drug_idx"]
    test_target_idx = val_fold["target_idx"]
    interact_mat_test = test_set["drugTarget"]
    true_labels = interact_mat_test[test_drug_idx, test_target_idx]
    
    ensemble = AdaMargin(c, k1, k2, num_factors, lambda_reg, lambda_u, lambda_v, gamma, max_iter, num_models, seed)
    ensemble.train(train_set)
    pred_mat = ensemble.predict(test_set)
    pred_labels = pred_mat[test_drug_idx, test_target_idx]
    
    #score = perf_metric(true_labels, pred_labels) #this doesn't work if was called with "delayed" (instead make a direct call to aupr function) 
    score = aupr(true_labels, pred_labels)
    
    return score
    
  #If you want to use this function, uncomment joblib import (joblib git version must be installed, pip version has errors)
  def grid_search(self, training_test_list, param_range, val_fold_list, perf_metric, n_jobs=-1):

    param_c = self.make_range(param_range['c'])
    param_k1 = self.make_range(param_range['k1'])
    param_k2 = self.make_range(param_range['k2'])
    param_num_factors = self.make_range(param_range['num_factors'])
    param_lambda = self.make_range(param_range['lambda'])
    param_lambda_u = self.make_range(param_range['lambda_u'])
    param_lambda_v = self.make_range(param_range['lambda_v'])
    param_gamma = self.make_range(param_range['gamma'])

    max_iter = param_range['max_iter']
    num_models = param_range['num_models']
    
    num_folds = len(training_test_list)
    
    evals = Parallel(n_jobs=n_jobs)(delayed(self.eval_model)( training_test_list[fold_idx], val_fold_list[fold_idx], c, k1, k2, num_factors, lambda_reg, lambda_u, lambda_v, gamma, max_iter, num_models, self.seed, "aupr") for fold_idx in np.arange(num_folds) for c in param_c for k1 in param_k1 for k2 in param_k2 for num_factors in param_num_factors for lambda_reg in param_lambda for lambda_u in param_lambda_u for lambda_v in param_lambda_v for gamma in param_gamma)
    
    evals = np.array(evals)
    param_score_mat = evals.reshape(num_folds, -1) #num_folds x param_comb
    param_score_acum = np.sum(param_score_mat, axis=0)#sum over each column (fold) for every param comb.
    
    best_param_comb = np.argmax(param_score_acum)
    
    c_size = len(param_c)
    k1_size = len(param_k1)
    k2_size = len(param_k2)
    num_factors_size = len(param_num_factors)
    lambda_size = len(param_lambda)
    lambda_u_size = len(param_lambda_u)
    lambda_v_size = len(param_lambda_v)
    gamma_size = len(param_gamma)
    
    #decode index 1D -> 8D
    best_c_idx = np.floor_divide(best_param_comb, k1_size*k2_size*num_factors_size*lambda_size*lambda_u_size*lambda_v_size*gamma_size) 
    best_k1_idx = np.floor_divide(best_param_comb, k2_size*num_factors_size*lambda_size*lambda_u_size*lambda_v_size*gamma_size) % k1_size
    best_k2_idx = np.floor_divide(best_param_comb, num_factors_size*lambda_size*lambda_u_size*lambda_v_size*gamma_size) % k2_size
    best_num_factors_idx = np.floor_divide(best_param_comb, lambda_size*lambda_u_size*lambda_v_size*gamma_size) % num_factors_size
    best_lambda_idx = np.floor_divide(best_param_comb, lambda_u_size*lambda_v_size*gamma_size) % lambda_size
    best_lambda_u_idx = np.floor_divide(best_param_comb, lambda_v_size*gamma_size) % lambda_u_size
    best_lambda_v_idx = np.floor_divide(best_param_comb, gamma_size) % lambda_v_size
    best_gamma_idx = best_param_comb % gamma_size
    
    best_parameter = {}
    best_parameter['c'] = param_c[best_c_idx]
    best_parameter['k1'] = param_k1[best_k1_idx]
    best_parameter['k2'] = param_k2[best_k2_idx]
    best_parameter['num_factors'] = param_num_factors[best_num_factors_idx]
    best_parameter['lambda'] = param_lambda[best_lambda_idx]
    best_parameter['lambda_u'] = param_lambda_u[best_lambda_u_idx]
    best_parameter['lambda_v'] = param_lambda_v[best_lambda_v_idx]
    best_parameter['gamma'] = param_gamma[best_gamma_idx]
    best_parameter['max_iter'] = param_range['max_iter']
    best_parameter['num_models'] = param_range['num_models']

    best_score = param_score_acum[best_param_comb] / float(num_folds)

    return best_parameter, best_score


  def predict(self, test_set):
    
    return self.strong_learner / float(np.sum(self.base_learner_weights)) #normalized output

