import sys
import subprocess


def main(argv):
    
  #cv_setting_opts = ["1","2","3","4"]
  cv_setting_opts = ["1"]
  dataset_list = ['nr', 'nr_ext','gpcr', 'gpcr_ext', 'ic', 'ic_ext', 'e', 'e_ext']
  
  for cv_opt in cv_setting_opts:

    for dataset in dataset_list:
      output = subprocess.check_output(["python", "make_folds.py", dataset, cv_opt])


if __name__ == "__main__":
  sys.exit(main(sys.argv))
