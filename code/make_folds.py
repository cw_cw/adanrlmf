# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import argparse

try:
  from pathlib import Path
except ImportError:
  from pathlib2 import Path

def load_yamanishi(datasetName, folder):
  with open(os.path.join(folder, datasetName+"_admat_dgc.txt"), "r") as inf:
    #inf.next()
    inf.readline() #delete column names
    int_array = [line.strip("\n").split()[1:] for line in inf] #[1:] to delete row names

  datasetName = datasetName.replace("_ext", "")
  with open(os.path.join(folder, datasetName+"_simmat_dc.txt"), "r") as inf:  # the drug similarity file
    #inf.next()
    inf.readline()
    drug_sim = [line.strip("\n").split()[1:] for line in inf]

  with open(os.path.join(folder, datasetName+"_simmat_dg.txt"), "r") as inf:  # the target similarity file
    #inf.next()
    inf.readline()
    target_sim = [line.strip("\n").split()[1:] for line in inf]

  drugDrug = np.array(drug_sim, dtype=np.float64)      # drug similarity matrix
  targetTarget = np.array(target_sim, dtype=np.float64)  # target similarity matrix
  drugTarget = np.array(int_array, dtype=np.float64).T    # drug-target label matrix (drugs as rows and proteins as columns)

  dataset = {}

  dataset['drugDrug'] = drugDrug
  dataset['targetTarget'] = targetTarget
  dataset['drugTarget'] = drugTarget

  return dataset

def stratified_test_fold(interact_mat, cv_setting_op, test_set_ratio, seed):

  np.random.seed(seed)

  if(cv_setting_op == 1):
    test_fold = stratified_test_fold_by_pair(interact_mat, test_set_ratio)

  elif(cv_setting_op == 2):
    test_fold = stratified_test_fold_by_drug(interact_mat, test_set_ratio)

  elif(cv_setting_op == 3):
    test_fold = stratified_test_fold_by_target(interact_mat, test_set_ratio)

  elif(cv_setting_op == 4):
    test_fold = stratified_test_fold_by_drug_target(interact_mat, test_set_ratio)

  return test_fold

def stratified_test_fold_by_pair(interact_mat, test_set_ratio):

  interact_drug, interact_target = np.where(interact_mat == 1)
  non_interact_drug, non_interact_target = np.where(interact_mat == 0)

  interact_idxs = np.arange(len(interact_drug))
  num_samples = int(len(interact_drug) * test_set_ratio)
  interact_fold_idx = np.random.choice(interact_idxs, num_samples, replace=False)
  interact_fold_drug = interact_drug[interact_fold_idx]
  interact_fold_target = interact_target[interact_fold_idx]

  non_interact_idxs = np.arange(len(non_interact_drug))
  num_samples = int(len(non_interact_drug) * test_set_ratio)
  non_interact_fold_idx = np.random.choice(non_interact_idxs, num_samples, replace=False)
  non_interact_fold_drug = non_interact_drug[non_interact_fold_idx]
  non_interact_fold_target = non_interact_target[non_interact_fold_idx]

  fold_drug = np.concatenate((interact_fold_drug, non_interact_fold_drug) )
  fold_target = np.concatenate((interact_fold_target, non_interact_fold_target) )

  test_fold = {}
  test_fold["drug_idx"] = fold_drug
  test_fold["target_idx"] = fold_target

  return test_fold

def stratified_test_fold_by_drug(interact_mat, test_ratio=0.3):
  rowCompleteData, colCompleteData = np.where( np.isnan(interact_mat) == False )

  drug_list = list(set(rowCompleteData))
  drug_size = len(drug_list)
  test_size = int(drug_size * test_ratio)

  drug_to_test = np.random.choice(drug_list, test_size, replace=False)

  fold_idx = []
  for drug in drug_to_test:
    indices = np.where(rowCompleteData == drug)[0]
    fold_idx.extend(indices)

  fold_test = {}
  fold_test['drug_idx'] = rowCompleteData[fold_idx]
  fold_test['target_idx'] = colCompleteData[fold_idx]

  return fold_test

def stratified_test_fold_by_target(interact_mat, test_ratio=0.3):
  rowCompleteData, colCompleteData = np.where( np.isnan(interact_mat) == False )

  target_list = list(set(colCompleteData))
  target_size = len(target_list)
  test_size = int(target_size * test_ratio)

  target_to_test = np.random.choice(target_list, test_size, replace=False)

  fold_idx = []
  for target in target_to_test:
    indices = np.where(colCompleteData == target)[0]
    fold_idx.extend(indices)

  fold_test = {}
  fold_test['drug_idx'] = rowCompleteData[fold_idx]
  fold_test['target_idx'] = colCompleteData[fold_idx]

  return fold_test

#return a dictionary with indices for drug and target used for testing. The size
#of test set is controlled by test_ratio parameter.
def stratified_test_fold_by_drug_target(interact_mat, test_ratio=0.3):

  num_rows, num_cols = interact_mat.shape

  if( num_rows < num_cols ):
    smaller_side = num_rows
  else:
    smaller_side = num_cols

  test_fold_size = np.ceil( smaller_side * test_ratio )

  interact_drug, interact_target = np.where( interact_mat == 1 )
  non_interact_drug, non_interact_target = np.where( interact_mat == 0 )

  num_interaction_label = len(interact_drug)
  num_non_interaction_label = len(non_interact_drug)
  total_data = np.sum(np.isfinite(interact_mat))

  interaction_ratio = float(num_interaction_label) / total_data
  no_interaction_ratio = float(num_non_interaction_label) / total_data

  interact_fold_size = int(np.ceil( interaction_ratio * test_fold_size )) #ceil to get at least 1 interact pair
  non_interact_fold_size = int(np.ceil( no_interaction_ratio * test_fold_size ))

  intract_indices = np.arange(num_interaction_label)
  non_intract_indices = np.arange(num_non_interaction_label)

  interact_fold_indices = np.random.choice(intract_indices, interact_fold_size, replace=False)
  non_interact_fold_indices = np.random.choice(non_intract_indices, non_interact_fold_size, replace=False)

  interact_fold_drug = interact_drug[interact_fold_indices]
  interact_fold_target = interact_target[interact_fold_indices]

  non_interact_fold_drug = non_interact_drug[non_interact_fold_indices]
  non_interact_fold_target = non_interact_target[non_interact_fold_indices]

  fold_drug = np.concatenate((interact_fold_drug, non_interact_fold_drug) )
  fold_target = np.concatenate((interact_fold_target, non_interact_fold_target) )

  test_fold = {}
  test_fold["drug_idx"] = fold_drug
  test_fold["target_idx"] = fold_target

  return test_fold


# changes fold idxs to numpy nan from a copy of interaction matrix and return it.
# The way in which fold idxs are change depends on cv setting option.
def fold_split(interact_mat, fold , cv_setting_op):

  inteact_without_fold = interact_mat.copy()

  drug_idx = fold['drug_idx']
  target_idx = fold['target_idx']

  if(cv_setting_op == 1):
    inteact_without_fold[ drug_idx, target_idx ] = np.nan

  elif(cv_setting_op == 2): #deleting drug rows
    inteact_without_fold[drug_idx, :] = np.nan

  elif(cv_setting_op == 3): #deleting target cols
    inteact_without_fold[:, target_idx] = np.nan

  elif(cv_setting_op == 4): #deleting drug-target cols-rows
    inteact_without_fold[drug_idx, :] = np.nan
    inteact_without_fold[:, target_idx] = np.nan
  else:
    print("Incorrect cv_setting_op option")
    sys.exit(1)


  return inteact_without_fold


def stratified_k_fold(interact_mat, num_folds, cv_setting_op, seed):
  np.random.seed(seed)

  if(cv_setting_op == 1):
    folds = stratified_k_fold_by_pais(interact_mat, num_folds)

  elif(cv_setting_op == 2):
    folds = stratified_k_fold_by_drug(interact_mat, num_folds)

  elif(cv_setting_op == 3):
    folds = stratified_k_fold_by_target(interact_mat, num_folds)

  elif(cv_setting_op == 4):
    folds = stratified_k_fold_by_drug_target(interact_mat, num_folds)

  else:
    print("Incorrect cross-validation setting option")

  return folds


def stratified_k_fold_by_pais(drug_target, num_folds):
  interact_drug, interact_target = np.where(drug_target == 1)
  non_interact_drug, non_interact_target = np.where(drug_target == 0)

  num_interact = len(interact_drug)
  size_interact_fold = int( num_interact/ num_folds)
  interact_fold_leftover = num_interact % num_folds
  interact_indices = np.arange(num_interact)
  np.random.shuffle(interact_indices)

  num_non_iteract = len(non_interact_drug)
  size_non_interact_fold = int(num_non_iteract / num_folds)
  non_interact_fold_leftover = num_non_iteract % num_folds
  non_interact_indices = np.arange(num_non_iteract)
  np.random.shuffle(non_interact_indices)

  k_folds = []

  curr_pos_interact = 0
  curr_pos_non_interact = 0
  for fold_idx in range(num_folds):

    interact_step = size_interact_fold
    if interact_fold_leftover > 0:
      interact_step += 1
      interact_fold_leftover -= 1

    next_pos_interact = curr_pos_interact + interact_step

    interact_fold_idx = interact_indices[curr_pos_interact:next_pos_interact]

    interact_drug_fold = interact_drug[interact_fold_idx]
    interact_target_fold = interact_target[interact_fold_idx]

    curr_pos_interact = next_pos_interact


    non_interact_step = size_non_interact_fold
    if non_interact_fold_leftover > 0:
      non_interact_step += 1
      non_interact_fold_leftover -= 1

    next_pos_non_interact = curr_pos_non_interact + non_interact_step
    non_interact_fold_idx = non_interact_indices[curr_pos_non_interact:next_pos_non_interact]

    non_interact_drug_fold = non_interact_drug[non_interact_fold_idx]
    non_interact_target_fold = non_interact_target[non_interact_fold_idx]

    curr_pos_non_interact = next_pos_non_interact


    fold = {}
    fold["drug_idx"] = np.concatenate((interact_drug_fold, non_interact_drug_fold))
    fold["target_idx"] = np.concatenate((interact_target_fold, non_interact_target_fold))

    k_folds.append(fold)

  return k_folds


def stratified_k_fold_by_drug(drug_target, num_folds):
  dataset_drug_idxs, dataset_target_idxs = np.where( np.isnan(drug_target) == False )

  drug_list = list(set(dataset_drug_idxs))
  drug_size = len(drug_list)
  np.random.shuffle(drug_list)

  drug_per_fold = int(drug_size / num_folds)
  drug_leftover = drug_size % num_folds

  k_folds = []
  curr_pos = 0
  for fold_idx in range(num_folds):

    drug_step = drug_per_fold
    if drug_leftover > 0:
      drug_step += 1
      drug_leftover -= 1

    end_pos = curr_pos + drug_step
    drug_to_test = drug_list[curr_pos:end_pos]
    curr_pos = end_pos

    fold_idx = []
    for drug in drug_to_test:
      indices = np.where(dataset_drug_idxs == drug)[0]
      fold_idx.extend(indices)

    fold = {}
    fold['drug_idx'] = dataset_drug_idxs[fold_idx]
    fold['target_idx'] = dataset_target_idxs[fold_idx]

    k_folds.append(fold)

  return k_folds


def stratified_k_fold_by_target(drug_target, num_folds):
  dataset_drug_idxs, dataset_target_idxs = np.where( np.isnan(drug_target) == False )

  target_list = list(set(dataset_target_idxs))
  target_size = len(target_list)
  np.random.shuffle(target_list)

  target_per_fold = int(target_size / num_folds)
  target_leftover = target_size % num_folds

  k_folds = []
  curr_pos = 0
  for fold_idx in range(num_folds):

    target_step = target_per_fold
    if target_leftover > 0:
      target_step += 1
      target_leftover -= 1

    end_pos = curr_pos + target_step
    target_to_test = target_list[curr_pos:end_pos]
    curr_pos = end_pos

    fold_idx = []
    for target in target_to_test:
      indices = np.where(dataset_target_idxs == target)[0]
      fold_idx.extend(indices)

    fold = {}
    fold['drug_idx'] = dataset_drug_idxs[fold_idx]
    fold['target_idx'] = dataset_target_idxs[fold_idx]

    k_folds.append(fold)

  return k_folds


#balance positive labels between folds, moving positive samples from
# folds with more positive labels (givers) to folds with less than
# an average number of positve labels (receivers)
def balance_fold(drug_target, fold_list):

  num_label1_by_fold = []

  for fold in fold_list:
    drug_idx = fold["drug_idx"]
    target_idx = fold["target_idx"]
    fold_labels = drug_target[drug_idx, target_idx]
    num_label1_by_fold.append( len(np.where(fold_labels == 1)[0]) )

  num_label1_by_fold = np.array(num_label1_by_fold)
  avg_label1 = np.average(num_label1_by_fold)

  if(avg_label1 < 1.0): #there are less '1' labels than folds
    return False

  min_label_1 = np.floor(avg_label1)

  receiver_1_folds = np.where(num_label1_by_fold < min_label_1)[0]
  if(receiver_1_folds.size == 0): #no fold with less than min_label_1 labels
    return True  #  so, it's already balanced

  receiver_asc_sort = np.argsort(num_label1_by_fold[receiver_1_folds])
  receiver_sorted = receiver_1_folds[receiver_asc_sort] #fold with less labels 1  first

  giver_1_folds = np.where(num_label1_by_fold > min_label_1)[0]
  giver_desc_sort = np.argsort(num_label1_by_fold[giver_1_folds])[::-1]
  giver_sorted = giver_1_folds[giver_desc_sort] #fold with higher labels 1 first

  giver_idx = 0
  receiver_idx = 0

  to_give = num_label1_by_fold[giver_sorted] - min_label_1
  to_receive = min_label_1 - num_label1_by_fold[receiver_sorted]

  is_balanced = False
  while(not is_balanced):
    giver_fold = fold_list[ giver_sorted[giver_idx] ]
    receiver_fold = fold_list[ receiver_sorted[receiver_idx] ]

    giver_drugs = giver_fold["drug_idx"]
    giver_targets = giver_fold["target_idx"]
    giver_pos_idx = np.where(drug_target[giver_drugs, giver_targets] == 1)[0]

    if(to_receive[receiver_idx] > to_give[giver_idx]):
      give_amount = int(to_give[giver_idx])
    else:
      give_amount = int(to_receive[receiver_idx])

    to_give[giver_idx] -= give_amount
    to_receive[receiver_idx] -= give_amount
    given_indices = np.random.choice(giver_pos_idx, give_amount, replace=False)

    #add positive labels to receiver fold
    drug_receiver = receiver_fold["drug_idx"]
    target_receiver = receiver_fold["target_idx"]
    receiver_fold["drug_idx"] = np.append(drug_receiver, giver_drugs[given_indices])
    receiver_fold["target_idx"]= np.append(target_receiver, giver_targets[given_indices])

    #delete given positive labels from giver
    giver_fold["drug_idx"] = np.delete(giver_drugs, given_indices)
    giver_fold["target_idx"] = np.delete(giver_targets, given_indices)


    if(to_give[giver_idx] == 0):
      giver_idx += 1
    if(to_receive[receiver_idx] == 0):
      receiver_idx += 1
    if(np.sum(to_receive) == 0 ):
      is_balanced = True

  return True


#get setting 4 k-folds intersecting setting 2 (by drugs) and 3 (by targets)
#  folds. Finally a balance is made to ensure positive labels in each fold.
def stratified_k_fold_by_drug_target(drug_target, num_folds, max_try=50):
  #num_drugs, num_target = drug_target.shape

  for try_idx in range(max_try):

    folds_by_drug_list = stratified_k_fold_by_drug(drug_target, num_folds)
    folds_by_target_list = stratified_k_fold_by_target(drug_target, num_folds)

    k_fold = []

    for fold_idx in range(num_folds):
      fold = {}

      by_drug_fold = folds_by_drug_list[fold_idx]
      by_target_fold = folds_by_target_list[fold_idx]

      drugs_by_drug = by_drug_fold["drug_idx"]
      drugs_by_target = by_target_fold["drug_idx"]
      common_drugs = np.intersect1d(drugs_by_drug, drugs_by_target)

      targets_by_drug = by_drug_fold["target_idx"]
      targets_by_target = by_target_fold["target_idx"]
      common_targets = np.intersect1d(targets_by_drug, targets_by_target)

      intersect_labels = drug_target[ np.ix_(common_drugs, common_targets) ]
      non_nan_drug, non_nan_target = np.where(np.isfinite(intersect_labels))

      fold["drug_idx"] = common_drugs[non_nan_drug]
      fold["target_idx"] = common_targets[non_nan_target]

      k_fold.append(fold)

    is_balanced = balance_fold(drug_target, k_fold)
    if(is_balanced): #at least one positive label per fold
      break
      
  if(not is_balanced): 
    print("Error, after trying ", max_try, " times, it was imposible to balance folds. Try to increase max_try parameter or use other random seed." )
    sys.exit()
    
  return k_fold


def make_seeds(num_seeds = 10, start=5):

  seeds = np.logspace(start=start, stop=32, num=num_seeds, endpoint=False, base=2.0)
  seeds = [ np.uint32(seed) for seed in seeds ]

  return seeds


def parse_arguments(argv):

  _MIN_PARAMTERS_ = 2

  parser = argparse.ArgumentParser(prog="make_folds.py", usage='python %(prog)s <dataset_name> <cv_setting> [optional arguments]')

  parser.add_argument("dataset_name", metavar="dataset_name",
    choices=['nr', 'gpcr', 'ic', 'e','nr_ext', 'gpcr_ext', 'ic_ext', 'e_ext'],
    type=str,
    help="Dataset name to use for experimentation.")

  parser.add_argument("cv_setting", metavar="cv_setting",
    choices=[1,2,3,4],
    type=int,
    help="Setting for cross-validation. 1 to hide drug-target pairs in training phase, 2 hide rows, 3 hide columns and 4 hide rows and cols simultaneously.")

  parser.add_argument("--num_trials",
    default=10,
    type=int,
    help="Number of trials for cross-validation. Each trial is created using a different random seed. Default value 10.")

  parser.add_argument("--num_folds",
    default=10,
    type=int,
    help="Number of folds utilized in cross-validation procedure. Default value 10.")

  parser.add_argument("--test_set_ratio",
    default=0.1,
    type=float,
    help="Percentage of data splitted to test set. Default value 10.")

  parser.add_argument("--dataset_directory",
    default="../dataset/yamanishi/",
    type=str,
    help="Relative or absolute path to dataset directory.")

  if len(argv) < _MIN_PARAMTERS_:
    parser.print_help()
    sys.exit

  args = parser.parse_args()

  return args


def main(argv):

  args = parse_arguments(argv)
  cv_setting_op = args.cv_setting
  test_set_ratio = args.test_set_ratio
  num_trials = args.num_trials
  num_folds = args.num_folds
  dataset_name = args.dataset_name

  dataset = load_yamanishi(args.dataset_name, args.dataset_directory)

  seed_list = make_seeds(num_trials)

  cv_folder = "cv" + str(cv_setting_op)
  path_folder = os.path.join(cv_folder, dataset_name)
  os.makedirs(path_folder, exist_ok=True)
  for trial_idx in range(num_trials):

    file_name = "trial_" + str(trial_idx)
    file_path = os.path.join(path_folder, file_name)
    writting_file = open(file_path, 'w')
    #file with k+1 rows. First row is for training-test split. The remaining K are
    #  for K-fold crossvalidation.

    seed = seed_list[trial_idx]
    test_fold = stratified_test_fold(dataset["drugTarget"], cv_setting_op, test_set_ratio, seed)

    writting_file.write(",".join(map(str, test_fold["drug_idx"])))
    writting_file.write(";")
    writting_file.write(",".join(map(str, test_fold["target_idx"])))
    writting_file.write("\n")

    inner_interact_mat = fold_split(dataset["drugTarget"], test_fold, cv_setting_op)
    val_fold_list = stratified_k_fold(inner_interact_mat, num_folds, cv_setting_op, seed )

    for val_fold in val_fold_list:
      writting_file.write(",".join(map(str, val_fold["drug_idx"])))
      writting_file.write(";")
      writting_file.write(",".join(map(str, val_fold["target_idx"])))
      writting_file.write("\n")

    writting_file.close()


if __name__=="__main__":
  main(sys.argv)


