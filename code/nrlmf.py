# -*- coding: utf-8 -*-
'''
[1] Yong Liu, Min Wu, Chunyan Miao, Peilin Zhao, Xiao-Li Li, "Neighborhood Regularized Logistic Matrix Factorization for Drug-target Interaction Prediction".
'''
import numpy as np
import sys

from sklearn.metrics import precision_recall_curve
from sklearn.model_selection import ParameterGrid
from sklearn.gaussian_process import GaussianProcessRegressor



class NRLMF:

  def __init__(self, c = 5, k1 = 5, k2 = 5, num_factors = 10,
      lambda_d=0.625, lambda_t=0.625, lambda_u=0.1, lambda_v=0.1, gamma = 1.0,
      max_iter=100, seed = 42):

    self.c = int(c)  # importance level for positive observations
    self.k1 = int(k1)
    self.k2 = int(k2)
    self.num_factors = int(num_factors)
    self.lambda_d = 2. ** float(lambda_d)
    self.lambda_t = 2. ** float(lambda_t)
    self.lambda_u = 2. ** float(lambda_u)
    self.lambda_v = 2. ** float(lambda_v)
    self.gamma = 2. ** float(gamma)
    self.max_iter = int(max_iter)
    
    self.seed = seed
    self.prng = np.random.RandomState(seed)
    
    
  def set_parameters(self, parameters):

    self.c = int(parameters['c'])
    self.k1 = int(parameters['k1'])
    self.k2 = int(parameters['k2'])
    self.num_factors = int(parameters['num_factors'])
    self.lambda_d = 2. ** parameters['lambda']
    self.lambda_t = 2. ** parameters['lambda']
    self.lambda_u = 2. ** parameters['lambda_u']
    self.lambda_v = 2. ** parameters['lambda_v']
    self.gamma = 2. **parameters['gamma']
    self.max_iter = int(parameters['max_iter'])


  def logistic_function(self, x):
    return 1.0 / (1 + np.exp(-x))


  def make_range(self, parameter_range):
    return np.arange(parameter_range[0], parameter_range[1], parameter_range[2])


  def neighborhood_weighted(self, mat, sim_mat):
    return sim_mat.dot(mat)


  def AGD_optimization(self, seed):
    mean = 0.0
    std_d = 1.0 / np.sqrt(self.lambda_d)
    std_t = 1.0 / np.sqrt(self.lambda_t)
    
    self.U = np.sqrt(1/float(self.num_factors)) * self.prng.normal(size=(self.num_drugs, self.num_factors), loc = mean, scale=std_d)
    self.V = np.sqrt(1/float(self.num_factors)) * self.prng.normal(size=(self.num_targets, self.num_factors),loc = mean, scale=std_t)

    acum_drug_deriv = np.zeros((self.num_drugs, self.num_factors))
    acum_target_deriv = np.zeros((self.num_targets, self.num_factors))

    last_log = self.log_likelihood()

    for iter_idx in range(self.max_iter):
      #Calc of learning rate using AdaGrad
      drug_derivative = self.deriv( make_drug_deriv = True)
      acum_drug_deriv += np.square(drug_derivative)
      vec_step_size = self.gamma / np.sqrt(acum_drug_deriv)

      #Fix V and update U
      self.U += vec_step_size * drug_derivative

      target_derivative = self.deriv( make_drug_deriv = False)
      acum_target_deriv += np.square(target_derivative)
      vec_step_size = self.gamma / np.sqrt(acum_target_deriv)

      #Fix U and update V
      self.V += vec_step_size * target_derivative

      #check for minimal increment stop criteria
      curr_log = self.log_likelihood()

      delta_log = (curr_log-last_log)/abs(last_log)
      if abs(delta_log) < 1e-5:
          break
      last_log = curr_log

    return iter_idx


  def deriv(self, make_drug_deriv = True):

    uv_t = self.U.dot(self.V.T)
    P = self.logistic_function(uv_t)
    c_1_Y_1_P = (1 + (self.c - 1) * self.interact_mat) * P # ((c-1)Y + 1) * P

    if make_drug_deriv:
      vec_deriv = self.c * self.interact_mat.dot(self.V)
      vec_deriv -= c_1_Y_1_P.dot(self.V)
      vec_deriv -= self.lambda_d * self.U #LMF reg. term
      vec_deriv -= self.lambda_u * self.drug_lapacian.dot(self.U) #NRLMF reg. term

    else:
      vec_deriv = self.c * self.interact_mat.T.dot(self.U)
      vec_deriv -= c_1_Y_1_P.T.dot(self.U)
      vec_deriv -= self.lambda_t * self.V
      vec_deriv -= self.lambda_v * self.target_laplacian.dot(self.V)

    return vec_deriv


  def log_likelihood(self):

    #first equation term: c * y_{ij} * u_i * v_j^T
    u_vt = np.dot(self.U, self.V.T)
    first_term = self.c * self.interact_mat * u_vt
    loglik = np.sum(first_term)

    #second equation term: (1 + (c-1) y_{ij}) log( 1 + exp(u_i v_j^t )
    log_1_exp_u_vt = np.log(1 + np.exp(u_vt))
    second_term = (1 + (self.c -1) * self.interact_mat) * log_1_exp_u_vt
    loglik -= np.sum(second_term)

    #LMF regularized term (3th and 4th equation terms)
    loglik -= 0.5 * self.lambda_d * np.sum(np.square(self.U))
    loglik -= 0.5 * self.lambda_t * np.sum(np.square(self.V))

    #NRLMF drug neigbor regularized term (5th equation term)
    loglik -= 0.5 * self.lambda_u * np.sum(np.diag(self.U.T.dot(self.drug_lapacian).dot(self.U)))

    #NRLMF target neigbor regularized term (6th equation terms)
    loglik -= 0.5 * self.lambda_v * np.sum(np.diag(self.V.T.dot(self.target_laplacian).dot(self.V)))

    return loglik

  def log_likelihood_by_element(self):

    loglik = 0

    c_interact = c * self.interact_mat
    u_vt = self.U.dot(self.V.T)

    for drug_idx in self.num_drugs:
      for target_idx in self.num_targets:
        loglik =  c_interact[drug_idx, target_idx] * u_vt[drug_idx, target_idx]\
          -(1+ c_interact[drug_idx,target_idx] - self.interact_mat[drug_idx, target_idx])\
          *np.log(1 + np.exp(u_vt[drug_idx, target_idx]))\
          - 0.5* self.lambda_d * np.norm(self.U[drug_idx, :]) \
          - 0.5* self.lambda_t * np.norm(self.V[target_idx, :]) \
          - 0.5* self.lambda_u * np.sum(np.diag(self.U[drug_idx, :].T.dot(self.drug_lapacian).dot(self.U[drug_idx,:])))\
          - 0.5* self.lambda_v * np.sum(np.diag(self.V[target_idx, :].T.dot(self.target_laplacian).dot(self.V[target_idx,:])))\

    return loglik


  def construct_neighborhood(self, drug_sim_mat, target_sim_mat):

    self.drug_sim_zeroDiag = drug_sim_mat - drug_sim_mat*np.eye(self.num_drugs)
    self.target_sim_zeroDiag = target_sim_mat - target_sim_mat*np.eye(self.num_targets)

    #drug/target_k1_NN are referred as 'A' and 'B' respectively in the paper[1].
    drug_k1_NN = self.get_nearest_neighbors(self.drug_sim_zeroDiag, self.k1)
    self.drug_lapacian = self.laplacian_matrix(drug_k1_NN)

    target_k1_NN = self.get_nearest_neighbors(self.target_sim_zeroDiag, self.k1)
    self.target_laplacian = self.laplacian_matrix(target_k1_NN)


  def laplacian_matrix(self, S):
    colum_sum = np.sum(S, axis=0)
    row_sum = np.sum(S, axis=1)
    L = 0.5*( np.diag(colum_sum + row_sum) - ( S + S.T) ) # neighborhood regularization matrix

    return L


  def get_nearest_neighbors(self, S, nun_neighbors = 5):
    m, n = S.shape
    X = np.zeros((m, n))
    for i in range(m):
      ii = np.argsort(S[i, :])[::-1][:min(nun_neighbors, n)]
      X[i, ii] = S[i, ii]

    return X


  def train(self, training_set, parameters=None, test_set=None, test_fold=None):
    
    if(isinstance(parameters, dict)):
      self.set_parameters(parameters)

    self.drug_sim_mat = training_set['drugDrug']
    self.target_sim_mat = training_set['targetTarget']
    self.interact_mat = np.nan_to_num(training_set['drugTarget'])
    
    self.num_drugs, self.num_targets = self.interact_mat.shape

    interact_drugs, interact_targets = np.where(self.interact_mat > 0)
    self.train_drugs = set(interact_drugs.tolist())
    self.train_targets = set(interact_targets.tolist())

    self.construct_neighborhood(self.drug_sim_mat, self.target_sim_mat)

    self.AGD_optimization(self.seed)
    self.repair_latent_vectors(self.U, self.V)
    

  def repair_latent_vectors(self, U, V):
    drug_idxs = np.array(list(self.train_drugs))
    drugSim_zeroDiag_train = self.drug_sim_zeroDiag[:, drug_idxs]

    target_idxs = np.array(list(self.train_targets))
    targetSim_zeroDiag_train = self.target_sim_zeroDiag[:, target_idxs]

    drugs = range(self.num_drugs)
    targets = range(self.num_targets)

    drugs_to_repair = list(set(drugs) - self.train_drugs )
    targets_to_repair = list(set(targets) - self.train_targets )

    for drug_idx in drugs_to_repair:
      #get the k2 most similar drugs
      drug_k2_NN = np.argsort(drugSim_zeroDiag_train[drug_idx, :])[::-1][:self.k2]

      #get their corresponding similarity
      drug_k2_NN_sim = np.asmatrix(drugSim_zeroDiag_train[drug_idx, drug_k2_NN]) #dimension of 1 x k2

      #get the new latent vector as the weighted average of its k2 most similar
      #neighbors
      U[drug_idx, :] = drug_k2_NN_sim.dot(U[drug_idxs[drug_k2_NN], :])
      U[drug_idx, :] = U[drug_idx, :] / np.sum(drug_k2_NN_sim)

    for target_idx in targets_to_repair:
      target_k2_NN = np.argsort(targetSim_zeroDiag_train[target_idx, :])[::-1][:self.k2]
      target_k2_NN_sim = np.asmatrix(targetSim_zeroDiag_train[target_idx, target_k2_NN])
      V[target_idx, :] = target_k2_NN_sim.dot(V[target_idxs[target_k2_NN], :])
      V[target_idx, :] = V[target_idx, :] / np.sum(target_k2_NN_sim)


  def predict(self, test_set):
    u_vt = self.U.dot(self.V.T)  
    return self.logistic_function(u_vt)
 
  
  def param_optimization(self, training_test_list, param_range, val_fold_list, perf_metric, opt_method):
    
    if opt_method == "grid_search":
      return self.grid_search(training_test_list, param_range, val_fold_list, perf_metric)
    elif opt_method == "bo_mi":
      return self.bayesian_optimization_mi(training_test_list, param_range, val_fold_list, perf_metric)
    else:
      sys.exit(2)
 
 
  def make_range(self, parameter_range):
    return np.arange(parameter_range[0], parameter_range[1], parameter_range[2])
 
 
  #posible optimization: pre-calculate nearest neaighbours and give it as argument
  def eval_model(self, train_test_pair, val_fold, c, k1, k2, num_factors, lambda_reg, lambda_u, lambda_v, gamma, max_iter, perf_metric, seed):
    
    train_set, test_set = train_test_pair
    test_drug_idx = val_fold["drug_idx"]
    test_target_idx = val_fold["target_idx"]
    interact_mat_test = test_set["drugTarget"]
    true_labels = interact_mat_test[test_drug_idx, test_target_idx]
    
    learner = NRLMF(c, k1, k2, num_factors, lambda_reg, lambda_reg, lambda_u, lambda_v, gamma, max_iter, seed)
    learner.train(train_set)
    pred_mat = learner.predict(test_set)
    pred_labels = pred_mat[test_drug_idx, test_target_idx]
    
    score = perf_metric(true_labels, pred_labels) 
    # score = aupr(true_labels, pred_labels)
    
    return score
    
  
  def grid_search(self, training_test_list, param_range, val_fold_list, perf_metric):

    param_c = self.make_range(param_range['c'])
    param_k1 = self.make_range(param_range['k1'])
    param_k2 = self.make_range(param_range['k2'])
    param_num_factors = self.make_range(param_range['num_factors'])
    param_lambda = self.make_range(param_range['lambda'])
    param_lambda_u = self.make_range(param_range['lambda_u'])
    param_lambda_v = self.make_range(param_range['lambda_v'])
    param_gamma = self.make_range(param_range['gamma'])

    c_size = len(param_c)
    k1_size = len(param_k1)
    k2_size = len(param_k2)
    num_factors_size = len(param_num_factors)
    lambda_size = len(param_lambda)
    lambda_u_size = len(param_lambda_u)
    lambda_v_size = len(param_lambda_v)
    gamma_size = len(param_gamma)

    self.max_iter = param_range['max_iter']

    num_folds = len(training_test_list)

    num_param_comb = k1_size * k2_size * c_size * num_factors_size * lambda_size * lambda_u_size * lambda_v_size * gamma_size

    acum_score_by_param = np.zeros( num_param_comb )
    max_acum_score = -1.0

    for fold_idx  in range(num_folds):
      #Training data
      training, test = training_test_list[fold_idx]
      drug_sim_mat_train = training['drugDrug']
      target_sim_mat_train = training['targetTarget']
      
      self.interact_mat = np.nan_to_num(training['drugTarget'])

      self.drug_sim_mat = drug_sim_mat_train
      self.target_sim_mat = target_sim_mat_train
      
      self.num_drugs, self.num_targets = self.interact_mat.shape

      interact_drugs, interact_targets = np.where(self.interact_mat > 0)
      self.train_drugs = set(interact_drugs.tolist())
      self.train_targets = set(interact_targets.tolist())

      #Testing data
      interact_mat_test = test['drugTarget']
      
      val_fold = val_fold_list[fold_idx]
      test_drug_idx = val_fold["drug_idx"]
      test_target_idx = val_fold["target_idx"]

      true_labels = interact_mat_test[test_drug_idx, test_target_idx]

      for k1_idx in range(k1_size):
        self.k1 = param_k1[k1_idx]

        #possible optimization: get NN of highest k1 value
        self.construct_neighborhood(drug_sim_mat_train, target_sim_mat_train)

        for c_idx in range(c_size):
          self.c = param_c[c_idx]

          for num_factors_idx in range(num_factors_size):
            self.num_factors = param_num_factors[num_factors_idx]

            for lambda_idx in range(lambda_size):
              lambda_val = param_lambda[lambda_idx]
              self.lambda_d = 2. ** lambda_val
              self.lambda_t = 2. ** lambda_val

              for lambda_u_idx in range(lambda_u_size):
                self.lambda_u = 2. ** param_lambda_u[lambda_u_idx]

                for lambda_v_idx in range(lambda_v_size):
                  self.lambda_v = 2. ** param_lambda_v[lambda_v_idx]

                  for gamma_idx in range(gamma_size):
                    self.gamma = 2. ** param_gamma[gamma_idx]

                    self.AGD_optimization(self.seed)

                    for k2_idx in range(k2_size):
                      self.k2 = param_k2[k2_idx]

                      prediction_mat = self.predict(test)
                      prediction_test = prediction_mat[test_drug_idx, test_target_idx]
                      score = perf_metric(true_labels, prediction_test)

                      score_idx = k1_idx*c_size*num_factors_size*lambda_size*lambda_u_size*lambda_v_size*gamma_size*k2_size \
                        + c_idx*num_factors_size*lambda_size*lambda_u_size*lambda_v_size*gamma_size*k2_size \
                        + num_factors_idx*lambda_size*lambda_u_size*lambda_v_size*gamma_size*k2_size \
                        + lambda_idx*lambda_u_size*lambda_v_size*gamma_size*k2_size \
                        + lambda_u_idx*lambda_v_size*gamma_size*k2_size \
                        + lambda_v_idx*gamma_size*k2_size \
                        + gamma_idx*k2_size + k2_idx

                      acum_score_by_param[score_idx] += score

                      if(max_acum_score < acum_score_by_param[score_idx]):
                        max_acum_score = acum_score_by_param[score_idx]
                        best_k1_idx = k1_idx
                        best_k2_idx = k2_idx
                        best_c_idx = c_idx
                        best_num_factors_idx = num_factors_idx
                        best_lambda_idx = lambda_idx
                        best_lambda_u_idx = lambda_u_idx
                        best_lambda_v_idx = lambda_v_idx
                        best_gamma_idx = gamma_idx

    best_parameter = {}
    best_parameter['k1'] = param_k1[best_k1_idx]
    best_parameter['k2'] = param_k2[best_k2_idx]
    best_parameter['c'] = param_c[best_c_idx]
    best_parameter['num_factors'] = param_num_factors[best_num_factors_idx]
    best_parameter['lambda'] = param_lambda[best_lambda_idx]
    best_parameter['lambda_u'] = param_lambda_u[best_lambda_u_idx]
    best_parameter['lambda_v'] = param_lambda_v[best_lambda_v_idx]
    best_parameter['gamma'] = param_gamma[best_gamma_idx]
    best_parameter['max_iter'] = param_range['max_iter']

    best_score = max_acum_score / float(num_folds)

    return best_parameter, best_score
    
    
  def bayesian_optimization_mi(self, training_test_list, param_range, val_fold_list, perf_metric, gpmi_delta=1e-100):
    
    param_c = self.make_range(param_range['c'])
    param_k1 = self.make_range(param_range['k1'])
    param_k2 = self.make_range(param_range['k2'])
    param_num_factors = self.make_range(param_range['num_factors'])
    param_lambda = self.make_range(param_range['lambda'])
    param_lambda_u = self.make_range(param_range['lambda_u'])
    param_lambda_v = self.make_range(param_range['lambda_v'])
    param_gamma = self.make_range(param_range['gamma'])
    max_iter = param_range['max_iter']
    
    param_grid = {"c": param_c, "k1": param_k1, "k2": param_k2, "num_factors": param_num_factors, "lambda": param_lambda, "lambda_u": param_lambda_u, "lambda_v": param_lambda_v, "gamma": param_gamma, "max_iter": [max_iter]}
    
    param_comb = list(ParameterGrid(param_grid))
    param_comb_features = [list(param.values()) for param in param_comb]
    num_param_comb = len(param_comb)
    
    num_folds = len(training_test_list)
    fold_list = np.arange(num_folds)
    
    #First observation
    param_idx = self.prng.randint(num_param_comb)
    next_param = param_comb[param_idx]
    
    param_score_acum = 0.
    for fold_idx in np.arange(num_folds):
      param_score_acum += self.eval_model(training_test_list[fold_idx], val_fold_list[fold_idx], next_param["c"], next_param["k1"], next_param["k2"], next_param["num_factors"], next_param["lambda"], next_param["lambda_u"], next_param["lambda_v"], next_param["gamma"], next_param["max_iter"], perf_metric, self.seed)
        
    param_score = param_score_acum / float(num_folds)
    
    best_score = param_score
    best_param = next_param
    features = [param_comb_features[param_idx]]
    scores = [param_score]
    
    alpha = np.log(2./gpmi_delta)
    gpmi_gamma = 0.
    for iteartion in np.arange(num_param_comb - 1):
      
      gaussian_process = GaussianProcessRegressor()
      gaussian_process.fit(features, scores)
      mean, sig = gaussian_process.predict(param_comb_features, return_std=True)
      
      phi = np.sqrt(alpha) * (np.sqrt(sig ** 2 + gpmi_gamma) - np.sqrt(gpmi_gamma))
      
      next_param_idx = np.argmax(mean + phi)
      next_param = param_comb[next_param_idx]
      next_param_feature = param_comb_features[next_param_idx]
      
      gpmi_gamma += sig[next_param_idx] ** 2
      
      param_score_acum = 0.
      for fold_idx in np.arange(num_folds):
        param_score_acum += self.eval_model(training_test_list[fold_idx], val_fold_list[fold_idx], next_param["c"], next_param["k1"], next_param["k2"], next_param["num_factors"], next_param["lambda"], next_param["lambda_u"], next_param["lambda_v"], next_param["gamma"], next_param["max_iter"], perf_metric, self.seed)
        
      param_score = param_score_acum / float(num_folds)
      
      if param_score > best_score:
        param_score = best_score
        best_param = next_param
        
      if np.array_equal(next_param_feature, features[-1]):
        break #local optima found
        
      features.append(next_param_feature)
      scores.append(param_score)
  
    return best_param, best_score

