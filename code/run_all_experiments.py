import sys
import subprocess
import os

import numpy as np
import pandas as pd


def write_results(table, to_write_file):
  tex_string = table.to_latex(float_format=lambda x: '%10.3f' % x)

  to_write_file.write(tex_string)


def safe_save(table, file_name, directory):

  temp_name = directory + file_name + ".tmp"
  to_save_file = open(temp_name, 'w')

  write_results(table, to_save_file)

  to_save_file.flush()
  os.fsync(to_save_file.fileno() )
  to_save_file.close()

  os.rename(temp_name, directory + file_name)

def main(argv):

  num_seeds = 5
  num_folds = 10
  dataset_directory = "."
  save_output = "True"
  save_directory="."
  performance_metric = ['aupr']
  cv_type = ['k-fold']
  test_set_ratio = .1

  save_directory ="results/"

  learner_names = ["nrlmf", "adamf", "adanrlmf"]
  dataset_list = ['nr', "nr_ext", 'gpcr', "gpcr_ext", 'ic', "ic_ext", 'e', "e_ext"]
  #cv_setting_opts = [1,2,3,4]
  cv_setting_opts = [1]

  ################### make experiments ##############################

  result_table = []
  result_filename = "results"

  empty_dataframe = pd.DataFrame(index = dataset_list, columns = learner_names )
  empty_dataframe.index.name = "Dataset"
  empty_dataframe.fillna('NAN')

  for cv_opt_idx in range( len(cv_setting_opts) ):
    result_table.append(empty_dataframe)

    result_filename_cv = result_filename + "_cv" + str(cv_setting_opts[cv_opt_idx]) + ".tex"

    for learner in learner_names:

      for dataset in dataset_list:
        output = subprocess.check_output(["python", "experiment.py", learner, dataset, str(cv_setting_opts[cv_opt_idx]), '--save_output' ])

        output = output.decode("ascii").rstrip('\n')
        numbers = [float(i) for i in output.split(" ")  ]

        output = '{:.3f} ({:.3f})'.format(*numbers )

        result_table[cv_opt_idx].loc[dataset, learner] = output
        safe_save(result_table[cv_opt_idx], result_filename_cv, save_directory)


if __name__ == "__main__":
  sys.exit(main(sys.argv))
