import numpy as np
from sklearn.metrics import average_precision_score, precision_recall_curve
import matplotlib.pyplot as plt

def aupr(y_true, y_score, sample_weight=None):
  precision, recall, thresholds = precision_recall_curve(
      y_true, y_score, sample_weight=sample_weight)
  # Return the step function integral
  # The following works because the last entry of precision is
  # guaranteed to be 1, as returned by precision_recall_curve
  return -np.sum(np.diff(recall) * np.array(precision)[:-1])

class AdaMF:

  def __init__(self, num_factors=10, lambda_reg=0.625, gamma=0.01, max_iter=100, num_models = 5, seed=43):

    #base learner parameters
    self.num_factors = num_factors
    self.gamma = 10. ** gamma
    self.lambda_reg = 2. ** lambda_reg
    self.max_iter = int(max_iter)

    #ensemble paramters
    self.num_models = int(num_models)

    #General paramters
    self.prng = np.random.RandomState(seed)
    self.seed = seed #for debugging
    self.epsilon = 1e-7


  def init_sample_weights(self):

    self.sample_weights = 1./self.num_drugs #weights over user/drugs


  def construct_neighborhood(self, drug_sim_mat, target_sim_mat):

    self.drug_sim_zeroDiag = drug_sim_mat - drug_sim_mat*np.eye(self.num_drugs)
    self.target_sim_zeroDiag = target_sim_mat - target_sim_mat*np.eye(self.num_targets)

    #drug/target_k1_NN are 'A' and 'B' respectively in the paper[1].
    drug_k1_NN = self.get_nearest_neighbors(self.drug_sim_zeroDiag, self.k1)
    self.drug_lapacian = self.laplacian_matrix(drug_k1_NN)

    target_k1_NN = self.get_nearest_neighbors(self.target_sim_zeroDiag, self.k1)
    self.target_laplacian = self.laplacian_matrix(target_k1_NN)


  def laplacian_matrix(self, S):
    colum_sum = np.sum(S, axis=0)
    row_sum = np.sum(S, axis=1)
    L = 0.5*( np.diag(colum_sum + row_sum) - ( S + S.T) ) # neighborhood regularization matrix %simulate laplacian matrix for undirect graphs

    return L


  def get_nearest_neighbors(self, S, nun_neighbors = 5):
    m, n = S.shape
    X = np.zeros((m, n))
    for i in range(m):
      ii = np.argsort(S[i, :])[::-1][:min(nun_neighbors, n)]
      X[i, ii] = S[i, ii]

    return X


  def set_parameters(self, parameters):

    self.num_factors = int(parameters['num_factors'])
    self.lambda_reg = 2. ** parameters['lambda_reg']
    self.gamma = 10. **parameters['gamma']
    self.max_iter = int(parameters['max_iter'])

    self.num_models = int(parameters['num_models'])


  def log_likelihood(self, U, V):

    u_vt = U.dot(V.T)
    first_term = self.c * self.interact_mat * self.masked_weights * u_vt
    loglik = np.sum(first_term)

    drug_weights = np.ones(U.shape[0])
    target_weights = np.ones(V.shape[0])

    #second equation term: (1 + (c-1) y_{ij}) log( 1 + exp(u_i v_j^t )
    log_1_exp_u_vt = np.log(1 + np.exp(u_vt))
    second_term = (1 + (self.c - 1) * self.interact_mat) * log_1_exp_u_vt * self.masked_weights
    loglik -= np.sum(second_term)

    #LMF regularized term (3th and 4th equation terms) #TODO: ver cómo cambian los resultados usando las 2 lineas de abajo.
    loglik -= 0.5 * self.lambda_d * np.sum(np.square(U))
    loglik -= 0.5 * self.lambda_t * np.sum(np.square(V))

    #NRLMF drug neighborhood regularized term (5th equation term)
    loglik -= 0.5 * self.lambda_u * np.sum(np.diag(U.T.dot(self.drug_lapacian).dot(U)))

    #NRLMF target neighborhood regularized term (6th equation term)
    loglik -= 0.5 * self.lambda_v * np.sum(np.diag(V.T.dot(self.target_laplacian).dot(V)))

    return loglik


  def deriv(self, U, V, make_drug_deriv = True):

    uv_t = U.dot(V.T)
    P = self.logistic_function(uv_t)
    c_1_Y_1_P_M_W = ((self.c - 1) * self.interact_mat + 1) * P * self.masked_weights

    if make_drug_deriv:
      vec_deriv = self.c * (self.interact_mat * self.masked_weights).dot(V)
      vec_deriv -= c_1_Y_1_P_M_W.dot(V)
      vec_deriv -= self.lambda_d * U #LMF reg. term
      vec_deriv -= self.lambda_u * self.drug_lapacian.dot(U) #NRLMF reg. term

    else:
      vec_deriv = self.c * (self.interact_mat * self.masked_weights).T.dot(U)
      vec_deriv -= c_1_Y_1_P_M_W.T.dot(U)
      vec_deriv -= self.lambda_t * V
      vec_deriv -= self.lambda_v * self.target_laplacian.dot(V)

    return vec_deriv


  def AGD_optimization(self, U, V):
    acum_drug_deriv = np.zeros((self.num_drugs, self.num_factors))
    acum_target_deriv = np.zeros((self.num_targets, self.num_factors))

    last_log = self.log_likelihood(U, V)
    for iter_idx in range(self.max_iter):

      #Calc of learning rate for U using AdaGrad
      drug_derivative = self.deriv(U, V, make_drug_deriv = True)
      acum_drug_deriv += np.square(drug_derivative)
      vec_step_size = self.gamma / np.sqrt(acum_drug_deriv)

      #Fix V and update U
      U += vec_step_size * drug_derivative

      #Calc of learning rate for V using AdaGrad
      target_derivative = self.deriv(U, V, make_drug_deriv = False)
      acum_target_deriv += np.square(target_derivative)
      vec_step_size = self.gamma / np.sqrt(acum_target_deriv)

      #Fix U and update V
      V += vec_step_size * target_derivative

      #check for minimal increment stop criteria
      curr_log = self.log_likelihood(U, V)

      #print "current log_likelihood: ", curr_log

      delta_log = (curr_log-last_log)/float(abs(last_log))
      if abs(delta_log) < 1e-5:
          break
      last_log = curr_log

    return iter_idx


  def repair_latent_vectors(self, U, V):
    drug_idxs = np.array(list(self.train_drugs))
    drugSim_zeroDiag_train = self.drug_sim_zeroDiag[:, drug_idxs]

    target_idxs = np.array(list(self.train_targets))
    targetSim_zeroDiag_train = self.target_sim_zeroDiag[:, target_idxs]

    drugs = range(self.num_drugs)
    targets = range(self.num_targets)

    drugs_to_repair = list(set(drugs) - self.train_drugs )
    targets_to_repair = list(set(targets) - self.train_targets )

    for drug_idx in drugs_to_repair:
      #get the k2 most similar drugs
      drug_k2_NN = np.argsort(drugSim_zeroDiag_train[drug_idx, :])[::-1][:self.k2]

      #get their corresponding similarity
      drug_k2_NN_sim = np.asmatrix(drugSim_zeroDiag_train[drug_idx, drug_k2_NN]) #dimension of 1 x k2

      #get the new latent vector as the weighted average of its k2 most similar
      #neighbors
      U[drug_idx, :] = drug_k2_NN_sim.dot(U[drug_idxs[drug_k2_NN], :])
      U[drug_idx, :] = U[drug_idx, :] / np.sum(drug_k2_NN_sim)

    for target_idx in targets_to_repair:
      target_k2_NN = np.argsort(targetSim_zeroDiag_train[target_idx, :])[::-1][:self.k2]
      target_k2_NN_sim = np.asmatrix(targetSim_zeroDiag_train[target_idx, target_k2_NN])
      V[target_idx, :] = target_k2_NN_sim.dot(V[target_idxs[target_k2_NN], :])
      V[target_idx, :] = V[target_idx, :] / np.sum(target_k2_NN_sim)


  def logistic_function(self, x):
    return 1.0 / (1.0 + np.exp(-x))


  def progressive_evaluation(self, interact_test, auprc, drug_test_idxs = None, target_test_idxs = None):
    scores = []
    strong_learner = np.zeros((self.num_drugs, self.num_targets))
    true_labels = interact_test.ravel()

    acum_alpha = 0.0
    for base_learner_id in range(self.num_models):
      U = self.U_list[base_learner_id]
      V = self.V_list[base_learner_id]
      weak_prediction = self.logistic_function(U.dot(V.T))

      logit_pred = 0.5 * np.log((weak_prediction + self.epsilon)/(1.0 - weak_prediction + self.epsilon))

      alpha = self.base_learner_weights[base_learner_id]

      acum_alpha += alpha
      strong_learner += alpha * logit_pred

      strong_pred_1d = strong_learner.ravel() / acum_alpha
      norm_pred = strong_learner / acum_alpha

      if(not (isinstance(drug_test_idxs, list) or isinstance(drug_test_idxs, np.ndarray)) ):
        scores.append(aupr(true_labels, strong_pred_1d))
      else:
        scores.append(aupr(interact_test[drug_test_idxs, target_test_idxs], norm_pred[drug_test_idxs, target_test_idxs]))

    return  scores


  def train(self, training_set, parameters=None, test_set=None, test_fold=None):

    if(isinstance(parameters, dict)):
      self.set_parameters(parameters)

    self.M = np.isfinite(training_set['drugTarget'])

    self.interact_mat = np.nan_to_num(training_set['drugTarget'])
    drug_sim_mat = training_set['drugDrug']
    target_sim_mat = training_set['targetTarget']

    self.num_drugs, self.num_targets = training_set['drugTarget'].shape

    interact_drugs, interact_targets = np.where(self.interact_mat > 0)
    self.train_drugs = set(interact_drugs.tolist())
    self.train_targets = set(interact_targets.tolist())

    mean = 0.0
    std_d = 1.0 / np.sqrt(self.lambda_reg)
    std_t = 1.0 / np.sqrt(self.lambda_reg)

    self.U_list = [np.sqrt(1/float(self.num_factors)) * self.prng.normal(size=(self.num_drugs, self.num_factors), loc = mean, scale=std_d)  for i in range(self.num_models)]
    self.V_list = [np.sqrt(1/float(self.num_factors)) * self.prng.normal(size=(self.num_targets, self.num_factors), loc = mean, scale=std_t) for i in range(self.num_models)]


    self.base_learner_weights = np.zeros(self.num_models)
    self.strong_learner = np.zeros((self.num_drugs, self.num_targets))
    self.sample_weights = np.ones(self.num_drugs) #weights over user/drugs

    for base_learner_id in range(self.num_models):
      U = self.U_list[base_learner_id]
      V = self.V_list[base_learner_id]

      self.sample_weights /= np.sum(self.sample_weights)

      #classical MF update
      for sgd_iter in range(self.max_iter):
        error_score = self.interact_mat - U.dot(V.T)
        U = U + self.gamma * (self.num_drugs * error_score.dot(V) * self.sample_weights[:,np.newaxis] - self.lambda_reg * U) #original version has no regularization; TODO: test without reg
        V = V + self.gamma * (self.num_drugs * error_score.T.dot(self.sample_weights[:,np.newaxis] * U) - self.lambda_reg * V)

      prediction = U.dot(V.T)

      aupr_by_user= np.zeros(self.num_drugs)
      #weighted_aupr = 0.
      for user_idx in range(self.num_drugs):

        if np.sum(self.interact_mat[user_idx, :]) == 0:
          #value to be defined
          user_aupr = np.nan
        else:
          user_aupr = aupr(self.interact_mat[user_idx, :], prediction[user_idx, :])

        aupr_by_user[user_idx] = user_aupr
        #weighted_aupr += self.sample_weights[user_idx] * user_aupr

      no_nan_idxs = np.where(np.isfinite(aupr_by_user))[0] #Aupr value is not defined when there is no positive labes. To avoid this problem we inputate average aupr score.
      if no_nan_idxs.size != 0: #there is at least 1 row without positive label
        nan_idxs = np.where(np.isnan(aupr_by_user))[0]
        mean_aupr = np.mean(aupr_by_user[no_nan_idxs])
        aupr_by_user[nan_idxs] = mean_aupr

      weighted_aupr = np.sum(aupr_by_user * self.sample_weights)

      self.alpha = 0.5 * np.log( (1. + weighted_aupr + self.epsilon)/(1. - weighted_aupr + self.epsilon) )
      self.base_learner_weights[base_learner_id] = self.alpha
      self.strong_learner +=  self.alpha * prediction
      self.sample_weights = np.exp(- aupr_by_user)


  def make_range(self, parameter_range):
    return np.arange(parameter_range[0], parameter_range[1], parameter_range[2])


  #posible optimization: pre-calculate nearest neaighbours and give it as argument
  def eval_model(self, train_test_pair, val_fold, num_factors, lambda_reg, gamma, max_iter, num_models, seed, perf_metric):

    train_set, test_set = train_test_pair
    test_drug_idx = val_fold["drug_idx"]
    test_target_idx = val_fold["target_idx"]
    interact_mat_test = test_set["drugTarget"]
    true_labels = interact_mat_test[test_drug_idx, test_target_idx]

    ensemble = AdaMF(num_factors, lambda_reg, gamma, max_iter, num_models, seed)
    ensemble.train(train_set)
    pred_mat = ensemble.predict(test_set)
    pred_labels = pred_mat[test_drug_idx, test_target_idx]

    #score = perf_metric(true_labels, pred_labels) #this doesn't work (instead make a direct call to aupr function)
    score = aupr(true_labels, pred_labels)

    return score


  def param_optimization(self, training_test_list, param_range, val_fold_list, perf_metric, opt_method):

    return self.grid_search(training_test_list, param_range, val_fold_list, perf_metric)


  def grid_search(self, training_test_list, param_range, val_fold_list, perf_metric):

    param_num_factors = self.make_range(param_range['num_factors'])
    param_lambda_reg = self.make_range(param_range['lambda_reg'])
    param_gamma = self.make_range(param_range['gamma'])

    max_iter = param_range['max_iter']
    num_models = param_range['num_models']
    
    num_factors_size = len(param_num_factors)
    lambda_reg_size = len(param_lambda_reg)
    gamma_size = len(param_gamma)
    
    num_param_comb = num_factors_size * lambda_reg_size * gamma_size
    acum_score_by_param = np.zeros( num_param_comb )
    
    num_folds = len(training_test_list)
    
    mean = 0.0

    self.base_learner_weights = np.zeros(self.num_models)
    
    max_acum_score = -1.0
    for fold_idx  in range(num_folds):
      #Training data
      training, test = training_test_list[fold_idx]
      
      self.interact_mat = np.nan_to_num(training['drugTarget'])
      interact_mat_test = test["drugTarget"]
      self.num_drugs, self.num_targets = self.interact_mat.shape
      
      val_fold = val_fold_list[fold_idx]
      test_drug_idx = val_fold["drug_idx"]
      test_target_idx = val_fold["target_idx"]

      true_labels = interact_mat_test[test_drug_idx, test_target_idx]
      
      self.strong_learner = np.zeros((self.num_drugs, self.num_targets))
      
      for num_factors_idx in range(num_factors_size):
        self.num_factors = param_num_factors[num_factors_idx]

        for lambda_reg_idx in range(lambda_reg_size):
          self.lambda_reg = 2. ** param_lambda_reg[lambda_reg_idx]
          
          std_d = 1.0 / np.sqrt(self.lambda_reg)
          std_t = 1.0 / np.sqrt(self.lambda_reg)

          for gamma_idx in range(gamma_size):
            self.gamma = 10. ** param_gamma[gamma_idx]
    
            #adamf learning
            self.sample_weights = np.ones(self.num_drugs) #weights over user/drugs
            
            self.U_list = [np.sqrt(1/float(self.num_factors)) * self.prng.normal(size=(self.num_drugs, self.num_factors), loc = mean, scale=std_d)  for i in range(self.num_models)]
            self.V_list = [np.sqrt(1/float(self.num_factors)) * self.prng.normal(size=(self.num_targets, self.num_factors), loc = mean, scale=std_t) for i in range(self.num_models)]
            
            for base_learner_id in range(self.num_models):
              U = self.U_list[base_learner_id]
              V = self.V_list[base_learner_id]

              self.sample_weights /= np.sum(self.sample_weights)

              #classical MF update
              for sgd_iter in range(self.max_iter):
                error_score = self.interact_mat - U.dot(V.T)
                U = U + self.gamma * (self.num_drugs * error_score.dot(V) * self.sample_weights[:,np.newaxis] - self.lambda_reg * U)
                V = V + self.gamma * (self.num_drugs * error_score.T.dot(self.sample_weights[:,np.newaxis] * U) - self.lambda_reg * V)

              prediction = U.dot(V.T)

              aupr_by_user= np.zeros(self.num_drugs)
              for user_idx in range(self.num_drugs):

                if np.sum(self.interact_mat[user_idx, :]) == 0:
                  #value to be defined
                  user_aupr = np.nan
                else:
                  user_aupr = aupr(self.interact_mat[user_idx, :], prediction[user_idx, :])

                aupr_by_user[user_idx] = user_aupr
                #weighted_aupr += self.sample_weights[user_idx] * user_aupr

              no_nan_idxs = np.where(np.isfinite(aupr_by_user))[0]
              if no_nan_idxs.size != 0: #there is at least 1 row without positive label
                nan_idxs = np.where(np.isnan(aupr_by_user))[0]
                mean_aupr = np.mean(aupr_by_user[no_nan_idxs])
                aupr_by_user[nan_idxs] = mean_aupr

              weighted_aupr = np.sum(aupr_by_user * self.sample_weights)

              self.alpha = 0.5 * np.log( (1. + weighted_aupr + self.epsilon)/(1. - weighted_aupr + self.epsilon) )
              self.base_learner_weights[base_learner_id] = self.alpha
              self.strong_learner +=  self.alpha * prediction
              self.sample_weights = np.exp(- aupr_by_user)
            
            prediction_mat = self.strong_learner
            prediction_test = prediction_mat[test_drug_idx, test_target_idx]
            score = perf_metric(true_labels, prediction_test)

            score_idx = num_factors_idx*lambda_reg_size*gamma_size \
              + lambda_reg_idx*gamma_size + gamma_idx

            acum_score_by_param[score_idx] += score

            if(max_acum_score < acum_score_by_param[score_idx]):
              max_acum_score = acum_score_by_param[score_idx]
              best_num_factors_idx = num_factors_idx
              best_lambda_reg_idx = lambda_reg_idx
              best_gamma_idx = gamma_idx

    best_parameter = {}
    best_parameter['num_factors'] = param_num_factors[best_num_factors_idx]
    best_parameter['lambda_reg'] = param_lambda_reg[best_lambda_reg_idx]
    best_parameter['gamma'] = param_gamma[best_gamma_idx]
    best_parameter['max_iter'] = param_range['max_iter']
    best_parameter['num_models'] = param_range['num_models']
    
    best_score = max_acum_score / float(num_folds)

    return best_parameter, best_score


  def predict(self, test_set):
 
    return self.strong_learner / float(np.sum(self.base_learner_weights))
