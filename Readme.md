# Code for "Boosting Collaborative Filters for Drug-Target Interaction Prediction"

In-silico prediction of interactions between drugs and proteins has become a crucial step in pharmaceutical sciences to reduce the time and cost required for drug discovery and repositioning. Even if the problem may be approached using standard recommendation algorithms, the accurate prediction of unknown drug-target interactions has shown to be very challenging due to the relatively small number of drugs with information of their target proteins and viceversa. This issue has been recently circumvent using regularization methods that actively exploit prior knowledge regarding drug similarities and target similarities. In this paper, we show that an additional improvement in terms of accuracy can be obtained using an ensemble approach which learns to combine multiple regularized filters for prediction. Our experiments on eight drug-protein interaction datasets show that most of the time this method outperforms a single predictor and other recommender systems based on multiple filters but not specialized to the drug-target interaction prediction task.


Requirements
------------

### Python
Python 3.x is needed to run this code. 

### Python libraries
These python libraries are needed to run the code:<br>
− scikit-learn (0.19.0 or higher): http://scikit-learn.org/stable/<br>
− Numpy (1.14.0 or higher): http://www.numpy.org/<br>
− Pandas (0.22.0 or higher): http://pandas.pydata.org/<br>

Numpy and Pandas are part of scipy packages and can be installed from https://www.scipy.org/install.html </br>

DataSets
---------------
We use the standar dataset of Yamanishi et al. http://web.kuicr.kyoto-u.ac.jp/supp/yoshi/drugtarget/. In 
addition to this dataset, we incorporate an updated version created by Jongsoo Keum and Hojung Nam
https://github.com/GIST-CSBL/SELF-BLM/tree/master/updated%20dataset. Both datasets are easily accesible in 
dataset folder (files with suffix "_ext" correspond to the extended version).
 

Usage
-----
The basic usage is:<br>
```shell
$ python experiment.py <learner_name> <dataset_name> <cv_setting> [optional_args]
```

for example, using NRLMF in dataset "nuclear receptor" on "cross validation setting" 1:

```shell
$ python experiment.py nrlmf nr 1
```

The parameters for each learner are defined in experiment.py file (lines 367 to 437). 
The range of each parameter  is defined by a triplet (inf, sup, step), where "inf" is 
the minimun value, "sup" is the máximun value (non inclusive) and "step" is the distance
between two values. For example, [2, 10, 2] gives values [2, 4, 6, 8]. Regularization parameters
take this value as exponent (usually using base 2), so you only define the exponent range.

For more information about optional arguments you can type:
```shell
$ python experiment.py --help
```


Acknowledgement
---------------
This script was created based on PyDTI developed by Liu et al. PyDTI can be accessed from the following URL.<br>
https://github.com/stephenliu0423/PyDTI.git<br>

Moreover, we speed up the parameter optimization with the work of Tomohiro Ban et al. whose code can be found at.<br>
https://github.com/akiyamalab/BO-DTI<br>

This research was partially supported by PIIC-2018 program of DGIP from the Federico Santa María Technical University.

Contact
-------
If there is any question, you can mail me at cristian.orellanam@alumnos.usm.cl

References
----------
C. Orellana M., R. Ñanculef, and C. Valle, “Boosting collaborative filters for drug-target inter-
action prediction,” in Progress in Pattern Recognition, Image Analysis, Computer Vision, and
Applications (R. Vera-Rodriguez, J. Fierrez, and A. Morales, eds.), (Cham), pp. 212–220, Springer
International Publishing, 2019.
<br>
